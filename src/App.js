import React, { Component } from "react";
import "./App.css";
import Movies from "./components/movies";
import ListGroup from "./components/common/listGroup";
import { getMovies } from "./services/fakMovieService";
import { getGenres } from "./services/fakeGenreService";
import { paginate } from "./utilities/pagination";

class App extends Component {
  state = {
    movies: [],
    currentPage: 1,
    pageSize: 4,
    genres: [],
  };

  componentDidMount() {
    const genres = [{ name: "All Genres" }, ...getGenres()];
    this.setState({ movies: getMovies(), genres });
  }

  handleDelete = (movieId) => {
    const movies = this.state.movies.filter((movie) => movie._id !== movieId);

    this.setState({ movies });
  };

  handleLike = (movieId) => {
    const movies = this.state.movies.slice();
    movies.forEach((movie, index) => {
      if (movie._id === movieId) {
        movies[index] = { ...movie };
        movies[index].like = movies[index].like ? false : true;
      }
    });

    this.setState({ movies });
  };

  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  handleGenreSelect = (genreId) => {
    this.setState({ selectedGenreId: genreId, currentPage: 1 });
  };

  render() {
    const {
      pageSize,
      currentPage,
      movies: allMovies,
      genres,
      selectedGenreId,
    } = this.state;
    const filtered = selectedGenreId
      ? allMovies.filter((movie) => movie.genre._id === selectedGenreId)
      : allMovies;
    const movies = paginate(filtered, currentPage, pageSize);

    return (
      <main className="container">
        <h1 className="my-3">Movies App</h1>
        <div className="row mt-5">
          <div className="col-2">
            <ListGroup
              items={genres}
              selectedItem={this.state.selectedGenreId}
              onItemSelect={this.handleGenreSelect}
            />
          </div>
          <div className="col-10">
            <div className="container">
              <Movies
                movies={movies}
                onDelete={this.handleDelete}
                onLike={this.handleLike}
                total={filtered.length}
                currentPage={currentPage}
                pageSize={pageSize}
                onPageChange={this.handlePageChange}
              />
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default App;
