import _ from "lodash";

export function paginate(items, pageNumber, pageSize) {
  const startIndex = (pageNumber - 1) * pageSize;

  /*
   * convert items to a lodash wrapper object,
   * to be able to concatenate different
   * lodash methods in a single chain
   *
   * value->convert the lodash wrapper object to an array
   */

  return _(items).slice(startIndex).take(pageSize).value();
}
