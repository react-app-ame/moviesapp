import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";

const Pagination = (props) => {
  const { total, pageSize, currentPage, onPageChange } = props;
  const pages = Math.ceil(total / pageSize);

  // If pages=1, don't show pagination
  if (pages === 1) return null;

  // Get an array like [0, 1, 2, 3 ....]
  const pagesArray = _.range(1, pages + 1);

  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination justify-content-center">
        {pagesArray.map((page) => (
          <li
            key={page}
            className={page === currentPage ? "page-item active" : "page-item"}
          >
            <a className="page-link" onClick={() => onPageChange(page)}>
              {page}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

Pagination.propTypes = {
  total: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export default Pagination;
