import React, { Component } from "react";

class Movie extends Component {
  render() {
    const {
      _id,
      dailyRentalRate: rate,
      genre,
      numberInStock: stock,
      title,
      like,
    } = this.props.movie;

    return (
      <tr key={_id}>
        <td>{title}</td>
        <td>{Object.values(genre)[1]}</td>
        <td>{stock}</td>
        <td>{rate}</td>
        <td>
          <span
            className="cursor-pointer"
            onClick={() => this.props.onLike(_id)}
          >
            <i className={like ? "bi bi-heart-fill" : "bi bi-heart"}></i>
          </span>
        </td>
        <td>
          <button
            type="button"
            className="btn btn-danger"
            onClick={() => this.props.onDelete(_id)}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

export default Movie;
