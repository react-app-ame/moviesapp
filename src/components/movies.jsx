import React, { Component } from "react";
import Movie from "./movie";
import Pagination from "./common/pagination";

class Movies extends Component {
  checkMovieList() {
    return this.props.total === 0
      ? "There are no movies in the database"
      : `There are ${this.props.total} movies in the database`;
  }

  render() {
    return (
      <React.Fragment>
        {this.checkMovieList()}
        <table className="table table-hover my-5">
          <thead>
            <tr>
              <th>Title</th>
              <th>Genre</th>
              <th>Stock</th>
              <th>Rate</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.props.movies.map((movie) => (
              <Movie
                key={movie.id}
                movie={movie}
                onDelete={this.props.onDelete}
                onLike={this.props.onLike}
              />
            ))}
          </tbody>
        </table>
        <Pagination
          total={this.props.total}
          pageSize={this.props.pageSize}
          currentPage={this.props.currentPage}
          onPageChange={this.props.onPageChange}
        />
      </React.Fragment>
    );
  }
}

export default Movies;
